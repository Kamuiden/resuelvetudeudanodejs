'use strinct'
// Cargamos Express y definimos el puerto donde va a escuchar las peticiones
let app     = require('./app');
let port    = 3800; 

 /*
 * @Description: Con el método listen configuramos el puerto para que nuestro servidor escuche las peticiones
 */
 app.listen(port, ()=>{
    console.log('Servidor corriendo correctamente en http://localhost:3800');
 });

