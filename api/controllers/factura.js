'use strict'

// Definición de variables y constantes
let moment          = require('moment');
let rp              = require('request-promise');
var promiseWhile    = require('promise-while');
let Promise         = require('bluebird');
const moreResults   = 'Hay más de 100 resultados'; 
const idFact        = '4e25ce61-e6e2-457a-89f7-116404990967';
let endPoint        = 'http://34.209.24.195';
let numFact         =  0;
let contMonth       = 1;
let arrayResult     = [];
let monthPrev       = 0;
let endDatePrev     = '';
let contCallOuts    = 0;
let year            = 2017;
let dtJson;
let blnMoreResults;
let startDate;
let endDate;

/*
 * @Description: Método que nos provee el paquete promise-while, para poder realizar multiples call outs 
 * a través de las promesas (Evitar Callback Hell)  
 */
var promiseWhile = function(condition, action) {
    var resolver = Promise.defer();
    var loop = function() {
        if (!condition()) return resolver.resolve();
        return Promise.cast(action())
        .then(loop)
        .catch(resolver.reject);
    };
    process.nextTick(loop);
    return resolver.promise;
};

/*
 * @request Request desde la ruta de factura creada se realiza la petición.
 * @response Response respuesta que enviaremos al servidor cuando obtengamos el número de facturas para el año 2017
 * @Description: Método que nos provee el paquete promise-while, para poder realizar multiples call outs 
 * a través de las promesas (Evitar Callback Hell)  
 */
function consultarFacturas(request, response) {
    promiseWhile(function() {
        // Condiciones para seguir ejecutando peticiones
        if(contMonth == 13) {
            year        = 2018;
            contMonth   = 1;
        }
        if(year == 2017) {
            if(blnMoreResults) {
                /* Cuando se obtiene como resultado mayor a 100 registros, dividimos el mes en 15 días
                   y obtenemos el rango de fechas 
                */
                dtJson         = getMonthDateRange(year, contMonth, blnMoreResults);
            }else {
                // Por defecto vamos a ir obteniendo rangos de fechas mensuales
                dtJson         = getMonthDateRange(year, contMonth);
            }
        }
        console.log(dtJson);
        //Condición principal la cual ejecutara peticiones hasta que pasemos al año 2018
        return moment(dtJson.end).year() == year;
    }, function() {
        // Acciones cuando se ejecuta cada callout
        return rp(endPoint+'/facturas?id='+idFact+'&start='+dtJson.start+'&finish='+dtJson.end).then((numFactMes)=>{
            // Acumulamos los resultados obtenidos siempre y cuando se obtenga un valor entero
            if(numFactMes != moreResults && Number.isInteger(parseInt(numFactMes)))  {
                numFact += parseInt(numFactMes); 
                arrayResult.push(numFactMes);
                console.log('valor ---------------------------------->'+numFactMes);
                blnMoreResults = false;
            }else {
                // prendemos una bandera en true para identificar la respuesta mayor a 100 registros
                blnMoreResults = true;
                contMonth -= 1;
            }
            // Contador callouts realizados 
            contCallOuts += 1; 
        });
    }).then(function(result) {
        // Resultado después de ejecutar promise-while
        // Enviamos la respuesta al servidor con los datos encontrados
        response.status(200).send({
            totalFacturas: numFact,
            valoresEncontrados: arrayResult,
            totalCallouts: contCallOuts 
          });
    });
}

/*
 * @year Año del cual obtendremos los diferentes rangos de fechas según aplique
 * @month la variable mes va a ser seteada por un contador que va a aumentar de acuerdo a la respuesta del servicio.
 * @blnMoreResults variable para identificar cuando obtenemos más de 100 registros como respuesta
 * @Description: Método que nos provee el paquete promise-while, para poder realizar multiples call outs 
 * a través de las promesas (Evitar Callback Hell)  
 */
function getMonthDateRange(year, month, blnMoreResults=false) {
    // Si no obtenemos un valor entero dividimos el mes y devolvemos el rango de fechas
    if(blnMoreResults) {
        startDate = moment([year, month - 1]);
        endDate   = moment(startDate).add(15,'days');
    }else if(parseInt(month) == parseInt(monthPrev)) {
        // fecha inicial: agregamos un día 
        // fecha final:  fecha final del mes 
        startDate = endDate.add(1,'days');
        endDate   = moment(startDate).endOf('month');
        contMonth += 1;
    }else {
        // fecha inicial: fecha inicio de cada mes 
        // fecha final:  fecha final del mes 
        startDate = moment([year, month - 1]);
        endDate = moment(startDate).endOf('month');
        contMonth += 1;
    }
    monthPrev   = month;
    endDatePrev = endDate;
    return { start: startDate.format('YYYY-MM-DD'), end: endDate.format('YYYY-MM-DD') };
}
module.exports = {
    consultarFacturas
}
