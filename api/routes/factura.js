'use strict'

/* 
 * El paquete de Nodejs express nos permite generar un servidor y 
 * crear rutas para obtener respuestas de la petición enviada
*/
let express             = require('express');
let facturaController   = require('../controllers/factura');
let api                 = express.Router();

api.get('/consultarFacturas', facturaController.consultarFacturas);

module.exports = api;