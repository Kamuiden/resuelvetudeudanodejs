'use strict'

// obtener libreria necesaria para cargar express 
let express     = require('express');
let bodyParser  = require('body-parser');
let app         = express();

// el la estructura json en un objeto usable por js
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// Definimos rutas
let user_routes   = require('./routes/factura');

// Cargar middlewares

// reescribir rutas
app.use('/api', user_routes);

module.exports = app;